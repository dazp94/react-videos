import axios from "axios";

const KEY = "AIzaSyDFNl0--ABe83p_BkyadEgvVcscDBMt5V0";

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    maxResults: 5,
    key: KEY
  }
});
